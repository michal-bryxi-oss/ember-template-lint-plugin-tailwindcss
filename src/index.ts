import path from "path";
import { fileURLToPath } from "url";

import classOrder from "./rules/class-order";
import classWrap from "./rules/class-wrap";
import recommendedRules from "./config/recommended";

import validatePeerDependencies from "validate-peer-dependencies";

const __dirname = path.dirname(fileURLToPath(import.meta.url));

validatePeerDependencies(__dirname);

export const name = "ember-template-lint-plugin-tailwindcss";
export const rules = {
  "class-order": classOrder,
  "class-wrap": classWrap,
};
export const configurations = {
  recommended: {
    rules: recommendedRules,
  },
};

// This is needed for tests
export default { name, rules, configurations };

import type { FullClassWrapConfig } from "../types";
import type { AST } from "ember-template-recast";
import { ASTHelpers as helpers } from "ember-template-lint";
import { Rule } from "ember-template-lint";
import { createErrorMessage } from "../utils/create-error-message";
import { classWrap as DEFAULT_CONFIG } from "./_default-config";

const chunk = (list: string[], chunkSize = 1) => {
  const cache: string[][] = [];
  const temporary = [...list];
  if (chunkSize <= 0) return cache;
  while (temporary.length > 0) cache.push(temporary.splice(0, chunkSize));
  return cache;
};

export default class ClassOrder extends Rule {
  parseConfig(config: FullClassWrapConfig): FullClassWrapConfig {
    if (config === true) {
      return DEFAULT_CONFIG;
    }

    if (config && typeof config === "object") {
      return {
        classesPerLine:
          "classesPerLine" in config ? config.classesPerLine : undefined,
      };
    }

    const errorMessage = createErrorMessage(
      "class-wrap",
      [
        "  * boolean - `true` to enable / `false` to disable",
        "  * object -- An object with the following keys:",
        "    * `classesPerLine` -- integer|null: Put a line-wrap in the class attribute after N classes",
      ],
      config
    );

    throw new Error(errorMessage);
  }

  visitor(): { ElementNode: (node: AST.ElementNode) => void } {
    return {
      ElementNode: (node: AST.ElementNode) => {
        const classAttribute = helpers.findAttribute(node, "class");
        if (!classAttribute) {
          return;
        }

        let classes;
        switch (classAttribute.value.type) {
          case "MustacheStatement":
            return;
          case "ConcatStatement":
            return;
          case "TextNode":
            classes = classAttribute.value.chars;
            break;
        }

        const gap = " ".repeat(classAttribute.value.loc.start.column);
        const allClasses = classes.replace(/\s+/g, " ").split(" ");

        const wrappedClasses =
          this.config.classesPerLine > 0
            ? chunk(allClasses, this.config.classesPerLine)
            : [allClasses];

        const classesString = wrappedClasses
          .map((row, index) => (index === 0 ? "" : gap) + row.join(" "))
          .join("\n");

        if (this.mode === "fix") {
          classAttribute.value.chars = classesString;
        }

        if (classes !== classesString) {
          this.log({
            message: `HTML class attribute wrap is: '${classes}', but should be: '${classesString}'`,
            node: classAttribute,
            isFixable: true,
          });
        }
      },
    };
  }
}

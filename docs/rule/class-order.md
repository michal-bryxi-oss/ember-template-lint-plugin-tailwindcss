# class-order

:white_check_mark: The `extends: 'recommended'` property in a configuration file enables this rule.

Enforces `class` attribute sorting according to specific [rules](../../lib/config/recommended.js).
This improves readibility, ability to _grep_ through the code for repeating patterns and `--fix` for automatic class sorting.

When you enable this rule it will remove all line breaks. So you might want to check [class-wrap](./class-wrap.md) rule that will enforce some line wrapping.

## Examples

Assuming [default configuration](../../lib/rules/_default-config.js) this rule **forbids** the following:

```hbs
<div class="md:flex bg-white foo rounded-lg p-2">foo</div>
```

This rule **allows** the following:

```hbs
<div class="foo p-2 bg-white rounded-lg md:flex">foo</div>
            |   |                       |_ tailwind variants sorted by alphabet
            |   |_ tailwind classes sorted by internal class list
            |_ non-tailwind classes
```

## Configuration

* boolean - `true` to enable / `false` to disable
* object - Containing the following values:
  * `matchers` -- object: Collection of functions used to determine whether given classs belongs to given group; These are _added_ to the list of [default matchers](../../lib/rules/_default-config.js)
    * `key` -- string: Used as a reference in `groups[x].matchBy`
    * `value` -- function(item): Callback function where `item` represents an _HTML attribute class name_; function should return `true` if provided `argument` belongs to given group and `false` if not
  * `sorters` -- object: Collection of functions used to sort array of HTML classes; These are _added_ to the list of [default sorters](../../lib/rules/_default-config.js)
    * `key` -- string: Used as a reference in `groups[x].sortBy`
    * `value` -- function(a, b): Callback function that should return `-1` if `a < b` and `1` if `a > b`
  * `groups` -- array: Individual matching groups; This setting will _overwrite_ [default groups](../../lib/rules/_default-config.js)
    * object - Matching group
      * `matchBy`: Refers to respective function in `matchers` config; If this function returns `true` with given _HTML class_, it will be assigned to this group; If not, next group will be checked; And so on till the last group; This is important, because it allows you to write relatively simple `matchers`; If _none_ of the groups are matched the class will be **dropped**; So it's important to always have one _last group_ with `matchBy: "rest"`
    * `sortBy`: Refers to respective function in `sorters` config; This function will be used to sort the _HTML classes_ within this group
    * `order`: After the _HTML classes_ **within** groups are sorted, this configuration is used to determine the order in which individual groups are returned; Lower number means it will be returned earlier
  * `linebreakBetweenGroups` -- boolean:
    * `true`: add a forced linebreak between each group
    * `false`: do not add linebreak between each group
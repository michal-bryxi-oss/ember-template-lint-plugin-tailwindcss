import assert from "assert";
import { describe, it } from "vitest";
import interleave from "../../src/utils/interleave";
import { builders } from "ember-template-recast";

describe("interleave()", function () {
  it("should work", function () {
    const x = builders.text("x");
    const a = builders.text("a");
    const b = builders.text("b");
    const c = builders.text("c");

    assert.deepStrictEqual(interleave([], x), []);
    assert.deepStrictEqual(interleave([a], x), [a]);
    assert.deepStrictEqual(interleave([a, b], x), [a, x, b]);
    assert.deepStrictEqual(interleave([a, b, c], x), [a, x, b, x, c]);
  });
});

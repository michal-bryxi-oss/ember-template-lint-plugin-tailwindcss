import { describe, it } from "vitest";
import assert from "assert";
import {
  pickFunction,
  hintedLinebreakReconstruction,
  linebreakReconstruction,
  reconstructClassesString,
  simpleReconstruction,
} from "../../src/utils/reconstruct-classes-string";
import { stripIndent } from "common-tags";
import { builders } from "ember-template-recast";

/*
<div
  class="
    foo bar baz
    hover:lorem active:ipsum
  "
>
        ^-- indentation: 9
    ^------ indentation: 4
  ^-------- indentation: 2
`;
*/

const CLASS_ATTRIBUTE = builders.attr(
  "class",
  builders.text(`\n    foo bar baz\n    hover:lorem active:ipsum\n  `),
  { start: { line: 2, column: 2 }, end: { line: 5, column: 2 } }
);

/*
<div
  class="foo bar baz hover:lorem active:ipsum"
>
*/
const CLASSES_STRING = "foo bar baz hover:lorem active:ipsum";

/*
<div
  class="foo bar baz
         hover:lorem active:ipsum">
*/
const CLASSES_STRING_BR = stripIndent`
  foo bar baz
      hover:lorem active:ipsum`;

/*
<div
  class="
    foo bar baz
    hover:lorem active:ipsum
  "
>
*/
const CLASSES_STRING_HINTED =
  `\n` + `    foo bar baz\n` + `    hover:lorem active:ipsum`;

const LINEBREAK_BETWEEN_GROUPS = {
  indent: 2,
  onlyWithHint: <const>false,
};

const LINEBREAK_BETWEEN_GROUPS_HINTED = {
  indent: 2,
  onlyWithHint: `\n`,
};

const GROUPS = [
  ["foo", "bar", "baz"],
  ["hover:lorem", "active:ipsum"],
];

describe("reconstructClassesString util", function () {
  describe("simpleReconstruction()", function () {
    it("should get simpleReconstruction", function () {
      assert.deepStrictEqual(
        simpleReconstruction({
          groups: GROUPS,
          indent: 0,
          classAttribute: CLASS_ATTRIBUTE,
        }),
        CLASSES_STRING
      );
    });
  });

  describe("linebreakReconstruction()", function () {
    it("should get linebreakReconstruction", function () {
      assert.deepStrictEqual(
        linebreakReconstruction({
          groups: GROUPS,
          classAttribute: CLASS_ATTRIBUTE,
          indent: LINEBREAK_BETWEEN_GROUPS.indent,
        }),
        CLASSES_STRING_BR
      );
    });
  });

  describe("hintedLinebreakReconstruction()", function () {
    it("should get hintedLinebreakReconstruction", function () {
      assert.deepStrictEqual(
        hintedLinebreakReconstruction({
          groups: GROUPS,
          classAttribute: CLASS_ATTRIBUTE,
          indent: LINEBREAK_BETWEEN_GROUPS.indent,
        }),
        CLASSES_STRING_HINTED
      );
    });
  });

  describe("pickFunction()", function () {
    describe("hintedLinebreakReconstruction", function () {
      it("should pick hintedLinebreakReconstruction", function () {
        assert.strictEqual(
          pickFunction({
            classesString: CLASSES_STRING_HINTED,
            linebreakBetweenGroups: LINEBREAK_BETWEEN_GROUPS_HINTED,
          }),
          hintedLinebreakReconstruction
        );
      });
    });

    describe("linebreakReconstruction", function () {
      it("linebreakReconstruction normal", function () {
        assert.strictEqual(
          pickFunction({
            classesString: CLASSES_STRING,
            linebreakBetweenGroups: LINEBREAK_BETWEEN_GROUPS,
          }),
          linebreakReconstruction
        );
      });

      it("linebreakReconstruction with classesString hinted", function () {
        assert.strictEqual(
          pickFunction({
            classesString: CLASSES_STRING_HINTED,
            linebreakBetweenGroups: LINEBREAK_BETWEEN_GROUPS,
          }),
          linebreakReconstruction
        );
      });
    });

    describe("simpleReconstruction", function () {
      it("simpleReconstruction normal", function () {
        assert.strictEqual(
          pickFunction({
            classesString: CLASSES_STRING,
            linebreakBetweenGroups: false,
          }),
          simpleReconstruction
        );
      });

      it("simpleReconstruction with classesString hinted", function () {
        assert.strictEqual(
          pickFunction({
            classesString: CLASSES_STRING_HINTED,
            linebreakBetweenGroups: false,
          }),
          simpleReconstruction
        );
      });

      it("simpleReconstruction with config hinted", function () {
        assert.strictEqual(
          pickFunction({
            classesString: CLASSES_STRING,
            linebreakBetweenGroups: LINEBREAK_BETWEEN_GROUPS_HINTED,
          }),
          simpleReconstruction
        );
      });
    });
  });

  describe("reconstructClassesString()", function () {
    it("no linebreaks", function () {
      assert.strictEqual(
        reconstructClassesString({
          linebreakBetweenGroups: false,
          classesString: CLASSES_STRING,
          classesGroups: GROUPS,
          classAttribute: CLASS_ATTRIBUTE,
        }),
        CLASSES_STRING
      );
    });

    it("linebreak between groups", function () {
      assert.strictEqual(
        reconstructClassesString({
          linebreakBetweenGroups: LINEBREAK_BETWEEN_GROUPS,
          classesString: CLASSES_STRING,
          classesGroups: GROUPS,
          classAttribute: CLASS_ATTRIBUTE,
        }),
        CLASSES_STRING_BR
      );
    });

    it("no linebreaks", function () {
      assert.strictEqual(
        reconstructClassesString({
          linebreakBetweenGroups: LINEBREAK_BETWEEN_GROUPS_HINTED,
          classesString: CLASSES_STRING_HINTED,
          classesGroups: GROUPS,
          classAttribute: CLASS_ATTRIBUTE,
        }),
        CLASSES_STRING_HINTED
      );
    });
  });
});

import assert from "assert";
import { describe, it } from "vitest";
import { isHinted } from "../../src/utils/is-hinted";

describe("isHinted()", function () {
  it("should say no", function () {
    assert.deepStrictEqual(isHinted("", ""), false);
    assert.deepStrictEqual(isHinted("", `\n`), false);
    assert.deepStrictEqual(isHinted("foo bar", `\n`), false);
    assert.deepStrictEqual(isHinted(`foo\nbar`, `\n`), false);
  });

  it("should say yes", function () {
    assert.deepStrictEqual(isHinted(`\n`, `\n`), true);
    assert.deepStrictEqual(isHinted(`\nfoo bar`, `\n`), true);
    assert.deepStrictEqual(isHinted(`\nfoo\nbar`, `\n`), true);
  });
});

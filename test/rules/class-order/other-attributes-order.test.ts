import { generateRuleTests } from "../test-helpers";

generateRuleTests({
  name: "class-order",

  config: {},

  good: [
    `<div id='foo' class="text-text-light flex items-center w-full text-xs' ...attributes></div>`,
  ],

  bad: [
    {
      template: `<div id='foo' class="w-full text-xs text-text-light flex items-center" ...attributes></div>`,
      fixedTemplate: `<div id='foo' class="text-text-light flex items-center text-xs w-full" ...attributes></div>`,
      result: {
        message:
          "HTML class attribute sorting is: 'w-full text-xs text-text-light flex items-center', but should be: 'text-text-light flex items-center text-xs w-full'",
        line: 1,
        column: 14,
        isFixable: true,
        source: `class="w-full text-xs text-text-light flex items-center"`,
      },
    },
  ],
});

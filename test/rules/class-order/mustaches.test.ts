import { stripIndent } from "common-tags";
import { generateRuleTests } from "../test-helpers";

generateRuleTests({
  name: "class-order",

  config: {
    disableForMustaches: false,
    linebreakBetweenGroups: {
      onlyWithHint: `\n`,
      indent: 2,
    },
  },

  good: [
    `<div class={{this.fooA}}></div>`, // only mustache statement
    `<div class="{{this.fooB}}"></div>`, // single mustache inside quotes
    `<div class="tmp bg-white p-1 rounded-lg md:flex {{this.fooC}}"></div>`, // with mustache statement
    `<div class="{{this.mustacheA}} {{this.mustacheB}}"></div>`, // mustaches only
    stripIndent`
      <div
        class="
          {{this.mustacheC}}
          {{this.mustacheD}}
        "
      ></div>
    `, // mustaches only with hinted linebreaks
    stripIndent`
      <div>
        <div
          class="
            bazDoo
            gap-4 grid grid-cols-2
            lg:grid-cols-5 md:grid-cols-3 sm:grid-cols-3 xl:grid-cols-6
            {{if
              this.painted
              'bg-red-400'
              'bg-green-400'
            }}
            {{if this.icon 'text-red-400' 'text-green-400'}}
          "
        ></div>
      </div>
    `, // complex code example
  ],

  bad: [
    {
      template: `<div class="px-8 {{this.zebra}} my-8 {{this.lion}}"></div>`,
      fixedTemplate: `<div class="my-8 px-8 {{this.zebra}} {{this.lion}}"></div>`,
      result: {
        message: `HTML class attribute sorting is: 'px-8 {{this.zebra}} my-8 {{this.lion}}', but should be: 'my-8 px-8 {{this.zebra}} {{this.lion}}'`,
        line: 1,
        column: 5,
        isFixable: true,
        source: `class="px-8 {{this.zebra}} my-8 {{this.lion}}"`,
      },
    },
    {
      template: stripIndent`
        <div
          class="pt-2 pb-4 {{this.foo}}"
        >
          {{#each this.data as |chunks|}}
            {{#each chunks as |chunk|}}
              {{#if (this.shouldShowImage chunk)}}
                <p class="px-4">foo</p>
              {{else}}
                <p>bar</p>
              {{/if}}
            {{/each}}
          {{/each}}
        </div>
      `,
      fixedTemplate: stripIndent`
        <div
          class="pb-4 pt-2 {{this.foo}}"
        >
          {{#each this.data as |chunks|}}
            {{#each chunks as |chunk|}}
              {{#if (this.shouldShowImage chunk)}}
                <p class="px-4">foo</p>
              {{else}}
                <p>bar</p>
              {{/if}}
            {{/each}}
          {{/each}}
        </div>
      `,
      result: {
        message: stripIndent`
          HTML class attribute sorting is: 'pt-2 pb-4 {{this.foo}}', but should be: 'pb-4 pt-2 {{this.foo}}'
        `,
        line: 2,
        column: 2,
        isFixable: true,
        source: stripIndent`
            class="pt-2 pb-4 {{this.foo}}"
        `,
      },
    },
    {
      template: stripIndent`
        <div
          class="
            px-8
              {{if this.foo 'bg-red-600' 'bg-blue-800'}}
            my-8
          "
        ></div>
      `,
      fixedTemplate: stripIndent`
        <div
          class="
            my-8 px-8
            {{if this.foo 'bg-red-600' 'bg-blue-800'}}
          "
        ></div>
      `,
      result: {
        message: stripIndent`
          HTML class attribute sorting is: '
              px-8
                {{if this.foo 'bg-red-600' 'bg-blue-800'}}
              my-8
            ', but should be: '
              my-8 px-8
              {{if this.foo 'bg-red-600' 'bg-blue-800'}}
            '
        `,
        line: 2,
        column: 2,
        isFixable: true,
        source: `class="
    px-8
      {{if this.foo 'bg-red-600' 'bg-blue-800'}}
    my-8
  "`,
      },
    },
    {
      template: stripIndent`
        <div>
          <div>
            <button
              class="
                w-full px-3 py-2 text-left {{this.seal}} focus-within text-swatch-4 hover:bg-swatch-1-2
                {{this.penguin}}
              "
              type="button"
            >
              hello world
            </button>
          </div>
        </div>
      `,
      fixedTemplate: stripIndent`
        <div>
          <div>
            <button
              class="
                focus-within text-swatch-4
                px-3 py-2 text-left w-full
                hover:bg-swatch-1-2
                {{this.seal}}
                {{this.penguin}}
              "
              type="button"
            >
              hello world
            </button>
          </div>
        </div>
      `,
      result: {
        message: stripIndent`
          HTML class attribute sorting is: '
                  w-full px-3 py-2 text-left {{this.seal}} focus-within text-swatch-4 hover:bg-swatch-1-2
                  {{this.penguin}}
                ', but should be: '
                  focus-within text-swatch-4
                  px-3 py-2 text-left w-full
                  hover:bg-swatch-1-2
                  {{this.seal}}
                  {{this.penguin}}
                '
        `,
        line: 4,
        column: 6,
        isFixable: true,
        source: `class="
        w-full px-3 py-2 text-left {{this.seal}} focus-within text-swatch-4 hover:bg-swatch-1-2
        {{this.penguin}}
      "`,
      },
    },
    {
      template: stripIndent`
        <div
          class="
            mt-1
            focus:mt-2
            {{if this.foo
              "foo"
              "bar"
            }} {{if this.bar "ml-auto"}}
          "
        ></div>
      `,
      fixedTemplate: stripIndent`
        <div
          class="
            mt-1
            focus:mt-2
            {{if this.foo
              "foo"
              "bar"
            }}
            {{if this.bar "ml-auto"}}
          "
        ></div>
      `,
      result: {
        message: stripIndent`
          HTML class attribute sorting is: '
              mt-1
              focus:mt-2
              {{if this.foo
                "foo"
                "bar"
              }} {{if this.bar "ml-auto"}}
            ', but should be: '
              mt-1
              focus:mt-2
              {{if this.foo
                "foo"
                "bar"
              }}
              {{if this.bar "ml-auto"}}
            '
        `,
        line: 2,
        column: 2,
        isFixable: true,
        source: `class="
    mt-1
    focus:mt-2
    {{if this.foo
      "foo"
      "bar"
    }} {{if this.bar "ml-auto"}}
  "`,
      },
    },
  ],
});

generateRuleTests({
  name: "class-order",

  config: {
    disableForMustaches: false,
    linebreakBetweenGroups: {
      indent: 7,
    },
  },

  good: [
    `<div class={{this.fooA}}></div>`, // only mustache statement
    `<div class="{{this.fooB}}"></div>`, // single mustache inside quotes
    stripIndent`
        <div>
          <div
            class="bazDoo
                   gap-4 grid grid-cols-2
                   lg:grid-cols-5 md:grid-cols-3 sm:grid-cols-3 xl:grid-cols-6
                   {{if
                     this.painted
                     'bg-red-400'
                     'bg-green-400'
                   }}
                   {{if this.icon 'text-red-400' 'text-green-400'}}"></div>
        </div>
      `, // complex code example
  ],

  bad: [
    {
      template: `<div class="px-8 {{this.quack}} my-8 {{this.lion}}"></div>`,
      fixedTemplate: stripIndent`
        <div class="my-8 px-8
                    {{this.quack}}
                    {{this.lion}}"></div>`,
      result: {
        message: stripIndent`HTML class attribute sorting is: 'px-8 {{this.quack}} my-8 {{this.lion}}', but should be: 'my-8 px-8
            {{this.quack}}
            {{this.lion}}'`,
        line: 1,
        column: 5,
        isFixable: true,
        source: `class="px-8 {{this.quack}} my-8 {{this.lion}}"`,
      },
    },

    {
      template: stripIndent`
          <div
            class="pt-2 mt-1
                   focus:mt-2
                   {{if this.foo
                     "foo"
                     "bar"
                   }}
                   {{if this.bar "ml-auto"}}"></div>
        `,
      fixedTemplate: stripIndent`
          <div
            class="mt-1 pt-2
                   focus:mt-2
                   {{if this.foo
                     "foo"
                     "bar"
                   }}
                   {{if this.bar "ml-auto"}}"></div>
        `,
      result: {
        message: stripIndent`
            HTML class attribute sorting is: 'pt-2 mt-1
                     focus:mt-2
                     {{if this.foo
                       "foo"
                       "bar"
                     }}
                     {{if this.bar "ml-auto"}}', but should be: 'mt-1 pt-2
                     focus:mt-2
                     {{if this.foo
                       "foo"
                       "bar"
                     }}
                     {{if this.bar "ml-auto"}}'
          `,
        line: 2,
        column: 2,
        isFixable: true,
        source:
          "" +
          `class="pt-2 mt-1
         focus:mt-2
         {{if this.foo
           "foo"
           "bar"
         }}
         {{if this.bar "ml-auto"}}"`,
      },
    },
  ],
});
